
(function () {
  'use strict';

  // page >> prefs

  function saveOptions(e) {
    e.preventDefault();
    browser.storage.local.set({
      server: document.getElementById('server').value,
      username: document.getElementById('username').value,
      password: document.getElementById('password').value,
      title: document.getElementById('title').checked,
      tagging: document.getElementById('tagging').checked,
      sorting: document.getElementById('sorting').selectedIndex,
    });
  }

  // prefs >> page

  function restoreOptions() {
    browser.storage.local.get().then((prefs) => {
      document.getElementById('server').value =
        prefs.server || '';
      document.getElementById('username').value =
        prefs.username || '';
      document.getElementById('password').value =
        prefs.password || '';
      document.getElementById('title').checked =
        prefs.title || false;
      document.getElementById('tagging').checked =
        prefs.tagging || false;
      document.getElementById('sorting').selectedIndex =
        prefs.sorting || 0;
    });
  }

  document.addEventListener('DOMContentLoaded', restoreOptions);
  document.querySelector('#saveOptions').addEventListener(
    'click', saveOptions);
})();
