module.exports = {
  "env": {
      "browser": true,
      "es2021": true,
      "webextensions": true
  },
  "plugins": [
    "es-beautifier"
  ],
  "extends": [
    "eslint:recommended",
    "plugin:es-beautifier/standard"
  ],
  "parserOptions": {
      "ecmaVersion": 12,
      "sourceType": "script"
  },
  "rules": {
    "quotes": ["error", "single"],
  },
  "globals": {
  }
}
