
/* exported fetchNote getSelectedNoteId getTimestamp InitPost Note ocpAlert */

function getTimestamp() {
  var local = new Date();

  local.setMinutes(local.getMinutes() - local.getTimezoneOffset());
  var str = local.toISOString();
  return str.slice(0, 10) + ' ' + str.slice(11, 19);
}

function getSelectedId(elemId) {
  const { options } = document.getElementById(elemId);
  const idx = options.selectedIndex;
  return idx === -1 ? idx : options[idx].value;
}

function getSelectedNoteId() {
  return getSelectedId('notes');
}

class Init {
  constructor(method, headers) {
    this.method = method;
    this.headers = headers;
  }
}

class InitPost extends Init {
  constructor(method, headers, note) {
    super(method, headers);
    this.body = JSON.stringify(note);
  }
}

class Note {
  constructor(content, favorite) {
    this.content = content;
    this.favorite = favorite;
    this.id = -1;
  }
}

function ocpAlert(msg) {
  browser.notifications.create({
    'type': 'basic',
    'iconUrl': browser.extension.getURL('icons/owncloud-32.png'),
    'title': 'ocpad',
    'message': msg,
  });
}

function fetchNote(request, onSuccess) {
  fetch(request).then(function (response) {
    return response.json();
  }).then(function (data) {
    onSuccess(data);
  }).catch(function (err) {
    ocpAlert('ERROR: Failed to send a request to your endpoint: ' +
      err.message);
  });
}
