
/* global config */
/* global assignTag createTag deleteTag getSelectedNoteId getTaggedFiles
  getTags insertDummyTag unassignTag */
/* global fetchNote getTimestamp Init InitPost Note ocpAlert  */

(function () {
  'use strict';

  const api = '/index.php/apps/notes/api/v0.2/notes';

  var app = {

    notelist: [],
    taglist: [],
    systaglist: [],
    deleteMode: true,

    favorite: false,

    notes: document.getElementById('notes'),
    options: document.getElementById('notes').options,
    content: document.getElementById('content'),
    fav: document.getElementById('note_fav'),

    tags: document.getElementById('tags'),
    systemtags: document.getElementById('systemtags'),

    tagsearch: document.getElementById('tag-search'),
    tagbuttons: document.getElementById('tag-buttons'),

    sorting: document.getElementById('sorting'),
    tagging: document.getElementById('tagging'),

    notebuttons: document.getElementById('note-buttons'),
    tagdelbutton: document.getElementById('tag-del'),

    // Tags

    getKnownTag: function (tagname) {
      return this.systaglist.find(tag => tag.name === tagname);
    },

    searchByTag: function (tagname) {
      var knownTag = this.getKnownTag(tagname);

      if (knownTag) {
        // Remove untagged notes from current list
        getTaggedFiles(knownTag.id).then((taggedFiles) => {
          function isTagged(note) {
            return taggedFiles.includes(note.id);
          }
          this.notelist = this.notelist.filter(isTagged);
          this.refreshNotelist();
          ocpAlert(`${this.options.length} matching notes found.`);
        });
      }
    },

    resetTaglist: function () {
      this.taglist = [];
      insertDummyTag(this.taglist);
      this.refreshTaglist();
    },

    refreshTaglist: function () {
      this.tags.options.length = 0;
      this.taglist.forEach((tag) => {
        var item = document.createElement('option');

        item.text = tag.name;
        item.value = tag.id;
        this.tags.add(item);
      });
    },

    refreshSysTaglist: function () {
      this.systemtags.innerHTML = '';
      this.systaglist.forEach((tag) => {
        var item = document.createElement('option');

        item.text = tag.name;
        item.id = tag.id;
        this.systemtags.appendChild(item);
      });
    },

    getAssociatedTags: function (noteId) {
      getTags(noteId).then((taglist) => {
        this.taglist = taglist;
        this.tags.selectedIndex = -1;
        this.refreshTaglist();
      })
    },

    notifyTagAdd: function (tagname) {
      ocpAlert(`Note sucessfully tagged with '${tagname}'.`);
    },

    notifyTagDel: function (tagname) {
      if (this.deleteMode) {
        ocpAlert(`Tag '${tagname}' sucessfully deleted.`);
      } else {
        ocpAlert(`Tag '${tagname}' sucessfully removed from note.`);
      }
    },

    tagNote: function () {
      var tagname = this.tagsearch.value;
      var noteId;

      if (tagname && (noteId = getSelectedNoteId()) !== -1) {
        var knownTag = this.getKnownTag(tagname);

        if (knownTag) {
          // assign tag
          assignTag(noteId, knownTag.id).then((taglist) => {
            this.taglist = taglist;
            this.getAssociatedTags(noteId);
            this.notifyTagAdd(tagname);
          });
        } else {
          // unknown/create new tag & assign
          createTag(tagname, getSelectedNoteId())
            .then((taglist) => {
              this.getAssociatedTags(noteId);
              this.systaglist = taglist;
              this.refreshSysTaglist();
              this.notifyTagAdd(tagname);
            });
        }
      } // no tag
    },

    untagNote: function () {
      var tagname = this.tagsearch.value;
      var noteId = getSelectedNoteId();
      var knownTag = this.getKnownTag(tagname);

      if (knownTag) {
        if (noteId !== -1) {
          unassignTag(noteId, knownTag.id).then(() => {
            this.loadNotes();
            this.notifyTagDel(tagname);
          });
        } else {
          deleteTag(knownTag.id).then((taglist) => {
            this.tagsearch.value = '';
            this.systaglist = taglist;
            this.refreshSysTaglist();
            this.loadNotes();
            this.notifyTagDel(tagname);
          });
        }
      }
    },

    // Notes

    clearNote: function (timestamp = false) {
      this.notes.selectedIndex = '-1';
      this.fav.checked = false;
      this.content.value = timestamp ? getTimestamp() : '';
      this.content.focus();
    },

    newNote: function () {
      this.clearNote(config.timestamp);
    },

    deselectNote: function () {
      this.notes.selectedIndex = -1;
      this.content.value = '';
      this.favorite = false;
      this.setDeleteMode(true);
    },

    loadNotes: async function (tagId = -1) {
      this.deselectNote();

      let notelist = [];
      let response = await fetch(`${config.url}?exclude=content`, {
        method: 'GET',
        headers: config.headers,
      });

      if (response.ok) {
        notelist = await response.json();
        this.sortNotes(notelist);
      } else {
        ocpAlert('ERROR: Failed to send a request to your endpoint: ' +
          response.status);
        console.error('Error:', response.status);
      }

      if (tagId !== -1) {
        // Click on taglist. Reduce list to notes tagged by tagId
        getTaggedFiles(tagId).then((taggedFiles) => {
          function isTagged(note) {
            return taggedFiles.includes(note.id);
          }
          this.notelist = notelist.filter(isTagged);
          this.sortNotes(this.notelist);
          this.refreshNotelist();
        });
      } else {
        this.notelist = notelist;
        this.sortNotes(this.notelist);
        this.refreshNotelist();
        this.resetTaglist();
      }
      this.deleteMode = true;
    },

    deleteNote: function () {
      var idx;
      var sel = [];

      while ((idx = this.notes.selectedIndex) !== -1) {
        sel.push(this.options[idx].value);
        this.notes.remove(idx);
      }

      sel.forEach((id) => {
        fetchNote(new Request(config.url + '/' + id,
          new Init('delete'), config.headers), function () {});
      });

      this.clearNote();
      ocpAlert(sel.length + ' note(s) deleted.');
    },

    saveNote: function () {
      var count = 0;

      for (var opt of this.options) {
        if (opt.selected) {
          count++;
        }
      }

      if (count > 1) {
        ocpAlert('ERROR: Too many selections');
        return;
      }

      if (this.content.value === '') {
        ocpAlert('ERROR: No content');
        return;
      }

      var note = new Note(this.content.value, this.fav.checked);
      var id 	= this.getSelectedId();
      var request;

      if (id !== -1) { // update existing note
        note.id = id;
        request = new Request(config.url + '/' + id,
          new InitPost('PUT', config.headers, note));
      } else {
        request = new Request(config.url,
          new InitPost('POST', config.headers, note));
      }

      fetchNote(request, () => {
        this.reloadIndex();
        this.clearNote();
        ocpAlert('Note succesfully saved.');
      });
    },

    refreshNotelist: function () {
      this.options.length = 0;
      this.notelist.forEach((note) => {
        var item = document.createElement('option');

        item.text = note.title;
        item.value = note.id;
        this.notes.add(item);
      });
    },

    reloadIndex: function () {
      fetchNote(new Request(config.url + '?exclude=content',
        new Init('GET', config.headers)), (data) => {
        this.notelist = data;
        this.sortNotes(this.notelist);
        this.refreshNotelist();
      });
    },

    getSelectedId: function () {
      var idx = this.options.selectedIndex;

      return idx === -1 ? idx : this.options[idx].value;
    },

    selectNote: function (id) {
      if (id === '') return;

      fetchNote(
        new Request(config.url + '/' + id.toString(),
          new Init('GET', config.headers)),	(data) => {
          this.fav.checked = data.favorite;
          this.content.value = data.content;
          this.content.focus();

          if (config.tagging) {
            this.getAssociatedTags(data.id);
          }
        },
      );
      this.setDeleteMode(false);
    },

    sortNotes: function (notelist) {
      function sortByMTime(notelist) {
        notelist.sort((lhs, rhs) => rhs.modified - lhs.modified);
      }

      function sortByFav(notelist) {
        notelist.sort((lhs, rhs) => rhs.favorite - lhs.favorite);
      }

      function sortByTitle(notelist) {
        notelist.sort((lhs, rhs) => {
          if (rhs.title > lhs.title) return -1;
          if (rhs.title < lhs.title) return 1;
          return 0;
        });
      }

      switch (config.sorting) {
        default:
        case 0: sortByMTime(notelist); break;
        case 1: sortByTitle(notelist); break;
        case 2: sortByFav(notelist); break;
      }
    },

    // UI

    setDeleteMode: function (mode = this.deleteMode) {
      this.deleteMode = mode;
      this.tagdelbutton.textContent = mode ? 'Delete' : 'Untag';
    },

    disableTagging: function (nodelist) {
      nodelist.forEach(item => item.classList.add('no-tags'));
      this.notes.classList.remove('short');
      this.loadNotes();
    },

    enableTagging: function (nodelist) {
      nodelist.forEach(item => item.classList.remove('no-tags'));
      this.notes.classList.add('short');
    },

    changeTagging: function (tagging) {
      var nodelist = document.querySelectorAll('.tag-elem')

      if (tagging) {
        this.enableTagging(nodelist);
      } else {
        this.disableTagging(nodelist);
      }
      config.setTagging(tagging);

      var noteId;

      if (tagging && (noteId = getSelectedNoteId()) !== -1) {
        this.getAssociatedTags(noteId);
      } else {
        this.resetTaglist();
      }
    },

    changeSorting: function (idx) {
      this.deselectNote();
      this.resetTaglist();
      config.setSorting(idx);
      this.sortNotes(this.notelist);
      this.refreshNotelist();
    },

    dispatchButtons: function (id) {
      switch (id) {
        case 'note-new':
          this.newNote();
          break;

        case 'note-save':
          this.saveNote();
          break;

        case 'note-del':
          this.deleteNote();
          break;

        case 'tag-add':
          this.tagNote();
          break;

        case 'tag-del':
          this.untagNote();
          break;

        default:
          break;
      }
    },

    init: function () {
      config.url = config.server + api;
      config.headers = {
        'Authorization': `Basic ${btoa(`${config.username}:${config.password}`)}`,
        'Content-Type': 'application/json',
      };

      this.sorting.selectedIndex = config.sorting;
      this.sorting.addEventListener('change', (e) => {
        this.changeSorting(e.target.selectedIndex);
      });

      this.resetTaglist();
      this.tagging.checked = config.tagging;
      this.tagging.addEventListener('change', (e) => {
        this.changeTagging(e.target.checked);
      });
      this.changeTagging(config.tagging);

      this.reloadIndex();
      getTags().then((taglist) => {
        this.systaglist = taglist;
        this.refreshSysTaglist();
      }); // all tags

      this.notes.addEventListener('click', (e) => {
        this.selectNote((e.target.value));
      });

      this.tags.addEventListener('click', (e) => {
        this.loadNotes(Number(e.target.value));
      });

      this.tagsearch.addEventListener('select', (e) => {
        this.searchByTag(e.target.value);
      });

      this.tagbuttons.addEventListener('click', (e) => {
        this.dispatchButtons(e.target.id);
      });

      this.notebuttons.addEventListener('click', (e) => {
        this.dispatchButtons(e.target.id);
      });

      this.setDeleteMode();
    },

  }; // app

  config.load().then(() => {
    app.init();
  });
})();
