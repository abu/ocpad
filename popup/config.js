
/* exported config */

'use strict';

var config = {
  server: '',
  username: '',
  password: '',
  timestamp: true,
  sorting: 0,
  tagging: false,

  load: async function () {
    var prefs = await browser.storage.local.get();

    this.server = prefs.server || '';
    this.username = prefs.username || '';
    this.password = prefs.password || '';
    this.timestamp = prefs.title || false;
    this.sorting = prefs.sorting || 0;
    this.tagging = prefs.tagging || false;
  },

  setTagging: function (tagging) {
    browser.storage.local.set({
      tagging: this.tagging = tagging,
    });
  },

  setSorting: function (sorting) {
    browser.storage.local.set({
      sorting: this.sorting = sorting,
    });
  },

};
