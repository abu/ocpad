
.PHONY: dev build sign lint lint-ext install update

IGNORE=tools/ Makefile package*
ifdef WEB_EXT_API_KEY
ifdef WEB_EXT_API_SECRET
ifdef WEB_EXT_CHANNEL
SIGN-OK=true
endif
endif
endif

dev: lint lint-ext

build: dev
	npx web-ext build -i $(IGNORE) -o

sign:
ifdef SIGN-OK
	npx web-ext sign --channel=$(WEB_EXT_CHANNEL) -i $(IGNORE) \
	--api-key=$(WEB_EXT_API_KEY) --api-secret=$(WEB_EXT_API_SECRET)
else
	$(error ERROR: Cannot sign due to missing environment.)
endif

lint:
	npx eslint options/ popup/

lint-ext:
	npx web-ext lint -i node_modules/

install:
	npm install

update:
	npm update
