# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## Unreleased

- Nothing so far

## [v1.2.3](https://codeberg.org/abu/ocpad/releases/tag/v1.2.3) - 2022-07-25

### Fixed

- Disable tagging must refresh the notes list
- Fixed layout upon switch tagging enable/disable

## [v1.2.2](https://codeberg.org/abu/ocpad/releases/tag/v1.2.2) - 2021-04-28

### Added

* Tagging support: tagging/untagging notes, selecting notes by tags

## [v1.1.0](https://codeberg.org/abu/ocpad/releases/tag/v1.1.0) - 2020-12-09

### Added

* Select multiple items for deletion

## [v1.0.2](https://codeberg.org/abu/ocpad/releases/tag/v1.0.2) - 2020-04-01

### Changed

* Homed at codeberg.org, URL adjusted

### Fixed

* Bugfix reloading listbox

## [v1.0.1](https://codeberg.org/abu/ocpad/releases/tag/v1.0.1) - 2018-07-12

## [v1.0.0](https://codeberg.org/abu/ocpad/releases/tag/v1.0.0) - 2018-06-24

---
