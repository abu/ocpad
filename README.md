
# ocpad - Notepad for ownCloud

Add-on for managing notes with ownCloud.

![Version](https://img.shields.io/amo/v/ocpad)

## Features

* Add, edit and delete notes on your ownCloud account.
* Multiple select for deletion is supported.
* Select notes as favorite. (Star/Unstar in ownCloud)
* Optional: Sorting the list of notes from title, favorite or modification time.
* Optional: Setting current Timestamp as title for new notes.

## Requirements

* owncloud-Server with notes app installed. Uses RESTful API v0.2.
